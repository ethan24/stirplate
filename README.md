A program that reads in records from various input files and then outputs the
list with command line options to sort or filter them.

**Supported formats**

	    pipe:

	    First name | Last name | Book Title | Book Publication Date

	    slash:

	    Book Publication Date/First name/Last name/Book Title

	    csv:

	    Book Title, Last Name, First name, Book Publication Date  


**To Run**

`books.py [-h] [--filter FILTER] [--year] [--reverse]`

Show a list of books, alphabetical ascending by author's last name 
                                                                                                                           
**optional arguments:** 

	   -h, --help       show this help message and exit

	   --filter FILTER  show a subset of books, looks for the argument as a substring of any of the fields

	   --year           sort the books by year, ascending instead of default sort

	   --reverse        reverse sort    