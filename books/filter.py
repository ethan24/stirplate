"""
Filter class that uses regular expressions to search for a given string
in the values of each dictionary in a list
"""

import re


class Filter:
    """
    Input: List of dictionaries from ParseFactory
    Output: List of dictionaries with sub strings matching input
    """

    def __init__(self, input_list, search_string):
        self.input_list = input_list
        self.search_string = search_string
        self.search_pattern = re.compile(search_string, re.IGNORECASE)

    def regex_search(self, input_dict):
        """
        :param input_dict: A dictionary representation
         of a line from the input file
        :return: input_dict if search returns True, otherwise None
        """
        for value in input_dict.values():
            if re.search(self.search_pattern, value):
                return input_dict

    def filter(self):
        """
        :return: List of dictionaries returned by regex_search
        """
        return [d for d in self.input_list if self.regex_search(d)]
