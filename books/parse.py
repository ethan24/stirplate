"""
Parse data from files with different types of delimiters
"""


class Parse:
    """
    Input: filename, delimiter
    Output: List of dictionaries from file
    """

    def __init__(self, filename, delimiter=None):
        self.filename = filename
        self.lines = open(self.filename, 'r').readlines()
        self.delimiter = self._get_delimiter() \
            if delimiter is None else delimiter
        self.translation_dict = {',': {'title': 0, 'last_name': 1,
                                       'first_name': 2, 'year': 3},
                                 '|': {'title': 2, 'last_name': 1,
                                       'first_name': 0, 'year': 3},
                                 '/': {'title': 3, 'last_name': 2,
                                       'first_name': 1, 'year': 0}}

    def _get_delimiter(self):
        """
        :return: Delimiter used to split the file
        """
        delimiters = [',', '|', '/']
        for delim in delimiters:
            if len(self.lines[0].split(delim)) > 1:
                return delim

    def _split_by_delimiter(self):
        """
        :return: A two dimensional array (list of lists)
        Each element is a string.
        """
        return [[y.strip() for y in x.split(self.delimiter)]
                for x in self.lines]

    @staticmethod
    def _build_dict(line_list, lookup_dict):
        """
        Use lookup_dict to get the index of each data point in the list
        :param: line_list: a line of strings split from the input file
        lookup_dict: The dictionary in self.translation_dict
        corresponding to the file delimiter
        :return: A dictionary with keys 'last_name',
        'first_name', 'title', 'year'
        """
        return dict((k, line_list[lookup_dict.get(k)]) for k in lookup_dict)

    def build_dict_list(self):
        """
        Run _build_dict on each line in a list
        :return: A list of dictionaries,
        which will be passed to Sorter and/or Filter
        """
        lookup_dict = self.translation_dict[self.delimiter]
        line_lists = self._split_by_delimiter()
        return [self._build_dict(line, lookup_dict) for line in line_lists]


class ParseFactory:
    """
    Create multiple parsers
    """

    def __init__(self, filename_list):
        self.parsers = [Parse(filename) for filename in filename_list]

    def get_dicts(self):
        """
        For each member parser,
        get a list of dictionaries.
        Combine all into one list
        :return: list of dictionaries representing lines of all input files
        """
        result = []
        for parser in self.parsers:
            for line_dict in parser.build_dict_list():
                result.append(line_dict)
        return result
