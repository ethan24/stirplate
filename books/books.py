"""
Main entry point for books
"""
import os
import argparse
from parse import ParseFactory
from filter import Filter
from sorter import Sorter


package_directory = os.path.dirname(os.path.abspath(__file__))


def render_dict_list(dict_list):
    """
    :param dict_list: List of dictionaries which have
    been sorted and/or filtered
    :return: Print statement of each dictionary as a comma separated line
    """
    for output_dict in dict_list:
        print ', '.join([output_dict['last_name'], output_dict['first_name'],
                         output_dict['title'], output_dict['year']])


def main():
    """
    Main user entry point
    :return:
    """
    parser = argparse.ArgumentParser(description="Show a list of books, "
                                                 "alphabetical ascending"
                                                 " by author's last name")
    parser.add_argument('--filter', action='store', dest='FILTER',
                        help='show a subset of books, looks for the argument '
                             'as a substring of any of the fields')
    parser.add_argument('--year', action='store_true', dest='YEAR',
                        default=False,
                        help='sort the books by year, '
                             'ascending instead of default sort')
    parser.add_argument('--reverse',
                        action='store_true', dest='REVERSE',
                        default=False, help='reverse sort')
    args = parser.parse_args()

    factory = ParseFactory([os.path.join(package_directory, 'data', 'csv.csv'),
                            os.path.join(package_directory, 'data', 'pipe-3'),
                            os.path.join(package_directory, 'data', 'slash-3')])
    dict_list = factory.get_dicts()

    if args.FILTER:
        dict_list = Filter(dict_list, args.FILTER).filter()
    sorter = Sorter(dict_list, reverse=args.REVERSE, sort_by_year=args.YEAR)
    dict_list = sorter.sort()
    render_dict_list(dict_list)

if __name__ == '__main__':
    main()
