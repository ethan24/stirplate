import unittest
import os
from books import parse

# NOTE: Given the small size of the input values, I used the entire input as test. Otherwise, I would
#       have possibly limited the asserts to the first couple of lines


package_directory = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'data'))


class CsvParseTest(unittest.TestCase):

    def setUp(self):
        self.parse = parse.Parse(os.path.join(package_directory, 'csv.csv'))

    def testFileIOHandling(self):
        self.assertEqual(['Clean Code, Martin, Robert, 2008\n',
                          'The Art of Agile Development, Shore, James, 2008'], self.parse.lines)

    def testDelimiterIsDetermined(self):
        self.assertEqual(',', self.parse._get_delimiter())

    def testSplitLineByDelimiter(self):
        self.assertEqual([['Clean Code', 'Martin', 'Robert', '2008'],
                          ['The Art of Agile Development', 'Shore', 'James', '2008']],
                         self.parse._split_by_delimiter())

    def testBuildDictFromLine(self):
        line = ['Clean Code', 'Martin', 'Robert', '2008']
        self.assertEqual({'title': 'Clean Code',
                          'first_name': 'Robert',
                          'last_name': 'Martin',
                          'year': '2008'},
                         self.parse._build_dict(line, self.parse.translation_dict[',']))


class PipeParseTest(unittest.TestCase):

    def setUp(self):
        self.parse = parse.Parse(os.path.join(package_directory, 'pipe-3'))

    def testFileIOHandling(self):
        self.assertEqual(['Kent | Beck | Test-Driven Development | 2002\n',
                          'Kent | Beck | Implementation Patterns | 2007\n',
                          'Martin | Fowler | Refactoring | 1999\n',
                          'Fred | Brooks | The Mythical Man-Month | 1975\n'], self.parse.lines)

    def testDelimiterIsDetermined(self):
        self.assertEqual('|', self.parse._get_delimiter())

    def testSplitLineByDelimiter(self):
        self.assertEqual([['Kent', 'Beck', 'Test-Driven Development', '2002'],
                          ['Kent', 'Beck', 'Implementation Patterns', '2007'],
                          ['Martin', 'Fowler', 'Refactoring', '1999'],
                          ['Fred', 'Brooks', 'The Mythical Man-Month', '1975']],
                         self.parse._split_by_delimiter())

    def testBuildDictFromLine(self):
        line = self.parse._split_by_delimiter()[0]
        self.assertEqual({'title': 'Test-Driven Development',
                          'first_name': 'Kent',
                          'last_name': 'Beck',
                          'year': '2002'},
                         self.parse._build_dict(line, self.parse.translation_dict['|']))


class SlashParseTest(unittest.TestCase):

    def setUp(self):
        self.parse = parse.Parse(os.path.join(package_directory,'slash-3'))

    def testFileIOHandling(self):
        self.assertEqual(['2008/Douglas/Crockford/Javascript: The Good Parts\n',
                          '1993/Steve/McConnell/Code Complete\n',
                          '2002/Martin/Fowler/Patterns of Enterprise Application Architecture\n'],
                         self.parse.lines)

    def testDelimiterIsDetermined(self):
        self.assertEqual('/', self.parse._get_delimiter())

    def testSplitLineByDelimiter(self):
        self.assertEqual([['2008', 'Douglas', 'Crockford', 'Javascript: The Good Parts'],
                          ['1993', 'Steve', 'McConnell', 'Code Complete'],
                          ['2002', 'Martin', 'Fowler', 'Patterns of Enterprise Application Architecture']],
                         self.parse._split_by_delimiter())


    def testBuildDictFromLine(self):
        line = self.parse._split_by_delimiter()[0]
        self.assertEqual({'title': 'Javascript: The Good Parts',
                          'first_name': 'Douglas',
                          'last_name': 'Crockford',
                          'year': '2008'},
                         self.parse._build_dict(line, self.parse.translation_dict['/']))