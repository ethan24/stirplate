import unittest
from books import filter


class EmptyFilterTest(unittest.TestCase):

    def setUp(self):
        self.empty_filter = filter.Filter([{'first_name': 'Kent',
                                      'last_name':'Beck',
                                      'title': 'Test-Driven Development',
                                      'year': '2002'},
                                     {'first_name': 'Robert',
                                      'last_name':'Martin',
                                      'title': 'Clean Code',
                                      'year': '2008'}],
                                    'ethan')



    def testRegexSearchNotFound(self):
        self.assertEqual(None, self.empty_filter.regex_search({'first_name': 'Kent',
                                                         'last_name':'Beck',
                                                         'title': 'Test-Driven Development',
                                                         'year': '2002'}))

    def testRegexSearchFound(self):
        search_dict = {'first_name': 'Ethan',
                       'last_name':'Cowan',
                       'title': 'title',
                       'year': '1990'}
        self.assertEqual(search_dict, self.empty_filter.regex_search(search_dict))

    def testEmptyFilter(self):
        self.assertEqual([], self.empty_filter.filter())


class NonEmptyFilterTest(unittest.TestCase):

    def setUp(self):
        self.nonempty_filter = filter.Filter([{'first_name': 'Kent',
                                               'last_name':'Beck',
                                               'title': 'Test-Driven Development',
                                               'year': '2002'},
                                              {'first_name': 'Robert',
                                               'last_name':'Martin',
                                               'title': 'Clean Code',
                                               'year': '2008'}],
                                             'kent')

    def testRegexSearchFound(self):
        search_dict = {'first_name': 'Kent',
                       'last_name':'Beck',
                       'title': 'Test-Driven Development',
                       'year': '2002'}
        self.assertEqual(search_dict, self.nonempty_filter.regex_search(search_dict))

    def testRegexSearchNotFound(self):
        search_dict = {'first_name': 'Ethan',
                       'last_name':'Cowan',
                       'title': 'title',
                       'year': '1990'}
        self.assertEqual(None, self.nonempty_filter.regex_search(search_dict))

    def testNonEmptyFilter(self):
        self.assertEqual([{'first_name': 'Kent',
                          'last_name':'Beck',
                          'title': 'Test-Driven Development',
                          'year': '2002'}],
                         self.nonempty_filter.filter())
