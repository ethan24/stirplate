import unittest
from books import sorter

class SorterTest(unittest.TestCase):

    def setUp(self):
        self.sorter = sorter.Sorter([{'last_name': 'b'},
                                     {'last_name': 'c'},
                                     {'last_name': 'a'}])
        self.reverse_sorter = sorter.Sorter([{'last_name': 'b'},
                                             {'last_name': 'c'},
                                             {'last_name': 'a'}],
                                            reverse=True)
        self.year_sorter = sorter.Sorter([{'year': '2008'},
                                          {'year': '2012'},
                                          {'year': '1990'},
                                          {'year': '1998'}],
                                         sort_by_year=True)
        self.reverse_year_sorter = sorter.Sorter([{'year': '2008'},
                                                  {'year': '2012'},
                                                  {'year': '1990'},
                                                  {'year': '1998'}],
                                                 reverse=True, sort_by_year=True)

    def testDefaultSort(self):
        self.assertEqual([{'last_name': 'a'},
                          {'last_name': 'b'},
                          {'last_name': 'c'}], self.sorter.sort())

    def testReverseSort(self):
        self.assertEqual([{'last_name': 'c'},
                          {'last_name': 'b'},
                          {'last_name': 'a'}], self.reverse_sorter.sort())

    def testYearSort(self):
        self.assertEqual([{'year': '1990'},
                          {'year': '1998'},
                          {'year': '2008'},
                          {'year': '2012'}], self.year_sorter.sort())

    def testReverseYearSort(self):
        self.assertEqual([{'year': '2012'},
                          {'year': '2008'},
                          {'year': '1998'},
                          {'year': '1990'}], self.reverse_year_sorter.sort())