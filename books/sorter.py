"""
Sorter class takes inputs and sorts accordingly
"""

from operator import itemgetter


class Sorter:
    """
    Input: List of dictionaries, returned from ParseFactory or Filter
    Output: List of dictionaries sorted based on input parameters
    """

    def __init__(self, input_list, reverse=False, sort_by_year=False):
        self.input_list = input_list
        self.reverse = reverse
        self.sort_by_year = sort_by_year

    def sort(self):
        """
        :return: self.input_list, sorted by last name or year, and/or reversed
        """
        if self.sort_by_year:
            sort_key = 'year'
        else:
            sort_key = 'last_name'
        return sorted(self.input_list,
                      key=itemgetter(sort_key), reverse=self.reverse)
