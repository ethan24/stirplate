from oauth2client.client import GoogleCredentials
from googleapiclient.discovery import build


class Client:
    
    def __init__(self, project="aws-is-hard", zone="us-east1-a"):
        self.project = project
        self.zone = zone
        self.credentials = GoogleCredentials.get_application_default()
        self.compute = build('compute', 'v1', credentials=self.credentials)

    def list_instances(self):
        result = self.compute.instances().list(project=self.project, zone=self.zone).execute()
        return result['items']

    def create_instance(self, name="instance-1", machine_size="g1-small"):
        print self.zone
        source_disk_image = "projects/debian-cloud/global/images/debian-7-wheezy-v20150320"
        machine_type = "zones/%s/machineTypes/n1-standard-1" % self.zone
        config = {
            "name": name,
            "zone": "https://www.googleapis.com/compute/v1/projects/aws-is-hard/zones/us-central1-a",
            "machineType": "https://www.googleapis.com/compute/v1/projects/aws-is-hard/zones/us-central1-a/machineTypes/" + machine_size,
            "metadata": {
                "items": []
            },
            "tags": {
                "items": []
            },
            "disks": [
                {
                    "type": "PERSISTENT",
                    "boot": True,
                    "mode": "READ_WRITE",
                    "deviceName": name,
                    "autoDelete": True,
                    "initializeParams": {
                        "sourceImage": "https://www.googleapis.com/compute/v1/projects/debian-cloud/global/images/backports-debian-7-wheezy-v2015\
                        0423",
                        "diskType": "https://www.googleapis.com/compute/v1/projects/aws-is-hard/zones/us-central1-a/diskTypes/pd-standard"
                    }
                },
                {
                    "type": "PERSISTENT",
                    "mode": "READ_ONLY",
                    "zone": "https://www.googleapis.com/compute/v1/projects/aws-is-hard/zones/us-central1-a",
                    "source": "https://www.googleapis.com/compute/v1/projects/aws-is-hard/zones/us-central1-a/disks/disk-1",
                    "deviceName": "disk-1"
                }
            ],
            "canIpForward": False,
            "networkInterfaces": [
                {
                    "network": "https://www.googleapis.com/compute/v1/projects/aws-is-hard/global/networks/default",
                    "accessConfigs": [
                        {
                            "name": "External NAT",
                            "type": "ONE_TO_ONE_NAT"
                        }
                    ]
                }
            ],
            "description": "",
            "scheduling": {
                "preemptible": False,
                "onHostMaintenance": "MIGRATE",
                "automaticRestart": True
            },
            "serviceAccounts": [
                {
                    "email": "default",
                    "scopes": [
                        "https://www.googleapis.com/auth/devstorage.read_only",
                        "https://www.googleapis.com/auth/logging.write"
                    ]
                }
            ]
        }
        
        return self.compute.instances().insert(project=self.project,
                                               zone=self.zone,
                                               body=config).execute()


    def wait_for_operation(compute, project, zone, operation):
        sys.stdout.write('Waiting for operation to finish')
        while True:
            result = compute.zoneOperations().get(project=project,
                                                  zone=self.zone,
                                                  operation=operation).execute()

            if result['status'] == 'DONE':
                print "done."
                if 'error' in result:
                    raise Exception(result['error'])
                return result
            else:
                sys.stdout.write('.')
                sys.stdout.flush()
                time.sleep(1)

